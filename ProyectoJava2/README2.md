# NOTAS
Usuarios	1
Complejidad	Baja

Simulación de una carrera con naves en una pista, basada en F-Zero. Este programa que permite visualizar tanto la información de las naves que estan concursando como los datos que se actializan por segundo en la competencia(tiempo actual, distancia recorrida, velocidad instantanea, aceleración, porcentaje de daño en la nave, etc).

# Objetivo
Ejercitar la programación orientada a objetos, elaborar el proyecto a traves de la descripción por objetos: composición, agregación, herencia, etc. Además de contar con la implementacion de al menos una interfaz.
Crear una carrera de naves mostrando sus estados y reconociendo el ganador.

# Construido con
El presente proyecto, se elaboró en base al lenguaje de programación Java, junto con el IDE ("Eclipse IDE for Java Developers"). El paquete de desarrollo utilizado fue la versión 8 de JDK.
* [Java] - Lenguaje de programación utilizado.
* [Eclipse](https://icb.utalca.cl/~fduran/eclipse-java-2019-03-R-linux-gtk-x86_64.tar.gz) - IDE de Eclipse, alojado en el repositorio de la carrera, Ingeniería Civil en Bioinformática - Universidad de Talca.

# Prerrequisitos
Para ejecutar correctamente el simulador, se debe tener instalado en el equipo el paquete de desarrollo de Java (JDK), puesto que es una programación escrita en Java. En caso de no contar con él, escribir:
```
apt install openjdk-8-jre
```

# Obtención del Programa
Para obtener este programa el primer paso es clonar el presente repositorio con el comando "git clone https://...".
Para compilar el juego se puede hacer a través de eclipse o terminal; en el caso de ser eclipse debe ingresar al repositorio del juego a través de este programa y luego ejecutarlo en el main. Por otra parte si se hace por terminal hemos de escribir lo siguiente: 
```
javac Main
```
A continuación, para ejecutar el programa en sí, escribir en la terminal:
```
java Main
```

# Sobre el código
El presente proyecto de programación consiste en la simulación de la Super Carrera Intergaláctica F-Zero, mediante la programación orientada a objetos. El objetivo del programa o "simulador", es alcanzar la meta antes que las demás naves contra las que se compite. 
Al iniciar el programa se comienza a realizar preguntas al usuario sobre algunos de los datos basicos de las naves, como su patente (matricula), numero de la nave, y otros más, esto se repite dos veces pues 2 naves estaran concursando.

Las naves que ingresan a la pista (clases Avion) ya han sido construidas con ciertas caracteristicas heredadas (de Nave), como: un estanque, odometro y un motor. En la clase estanque se hace un sorteo aleatorio para seleccionar, entre tres tipos, el combustible que utilizará cada nave, esta información le sirve al motor para calcular la aceleracion de la nave. Mientras que odometro se encarga mayormente de las variables que se estaran modificando a lo largo de la carrera, como lo es la distancia recorrida.

Las naves, como se mencionó anteriormente, ingresaran a la pista a medida que se llenen sus datos y cuando estén todas en posición comenzará la carrera. En primer lugar tenemos la presentación de los competidores en donde se muestra la información relacionada a estos, luego tenemos el arranque de las naves que es cuando se empieza a mostrar por terminal los mensajes que informan el estado de cada nave (velocidad, distancia recorrida, aceleración, estado del motor, etc) por unidad de tiempo, a esto se le agregan los comentarios por parte de un locutor (que naves lideran la competencia y advertencias sobre sus estados) y una opción un poco más visual del recorrido de las máquinas en relación a el intervalo recorrido respecto a la pista por la que corren. Hace falta mencionar que las naves requieren de atención para completar la carrera sin daños importantes por lo que es vital su cinstante revision y reparacion, estos arreglos los lleva a cabo una nave asistente o auxiliar. Finalmente, una vez que todas las naves lleguen a la meta, el locutor se encarga de transmitir los Lugares (premiación) respecto a los resultados de la carrera.

# Errores del programa
- Falta implementar los comentarios del locutor respecto a quien lidera la carrera
- Falta considerar el caso de que las naves empaten

# Autores
Estudiantes de segundo año de Ingeniería Civil en Bioinformática.
* **Rachell Aravena**
* **Constanza Valenzuela**
