package carrera_visual;
//Librerias utilizadas
import javax.swing.ImageIcon;//Imagenes
import javax.swing.JButton;//Boton inicio
import javax.swing.JFrame;//Ventana
import javax.swing.JLabel;//Labels

public class Pista extends JFrame{
	
	JLabel lbNaver, lbNavev, lbNavec, lbCarreteraFondo;
	JButton btnInicio;
	
	
	//Constructor
	public Pista(){
		//Metodo super es para sobreescribir en JFrame
		//Nombre en ventana
		super("Carrera Galáctica");
		
		//Ordenar imagenes en las posiciones q yo quiero
		getContentPane().setLayout(null);
		
		//Señalizar labels

		lbCarreteraFondo = new JLabel("CarreteraFondo");
		getContentPane().add(lbCarreteraFondo);//Agrega al frame
		//Imagen
		lbCarreteraFondo.setIcon(new ImageIcon(getClass().getResource("estrellas.gif")));
		//Los 2 primeros numeros son el frame, 
		//2 ultimos para tamaño imagen
		lbCarreteraFondo.setBounds(100, 0, 500, 500);
		
		lbNaver = new JLabel("Naver");
		//getContentPane().add(lbNaver); --> comentado porq ahora va a la imagen de la pista
		lbCarreteraFondo.add(lbNaver);
		lbNaver.setIcon(new ImageIcon(getClass().getResource("roja_opt.png")));
		lbNaver.setBounds(0, 50, 100, 100);
		
		lbNavev = new JLabel("Navev");
		//getContentPane().add(lbNavev);
		lbCarreteraFondo.add(lbNavev);
		lbNavev.setIcon(new ImageIcon(getClass().getResource("verde_opt.png")));
		lbNavev.setBounds(0, 200, 100, 100);
		
		lbNavec = new JLabel("Navec");
		//getContentPane().add(lbNavec);
		lbCarreteraFondo.add(lbNavec);
		lbNavec.setIcon(new ImageIcon(getClass().getResource("cohete_opt.png")));
		lbNavec.setBounds(0, 350, 100, 100);
		
		
		//Boton inicio
		btnInicio = new JButton("Inicio");
		getContentPane().add(btnInicio);
		btnInicio.setBounds(0, 0, 100, 50);
		
		
		//Operacion de salida, o se ejecutaria infinitamente
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		//Posicion inicial, ancho y alto ventana
		setBounds(0, 0, 600, 500);
		//Visibilidad ventana
		setVisible(true);
		//No hay modificacion del tamaño de la ventana por el usuario
		setResizable(false);
	}

}