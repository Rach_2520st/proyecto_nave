# NOTAS
Usuarios	1
Complejidad	Baja

Simulación de una carrera con naves en una pista, basada en F-Zero. Este programa que permite visualizar tanto la información de las naves que estan concursando como los datos que se actializan por segundo en la competencia(tiempo actual, distancia recorrida, velocidad instantanea, aceleración, porcentaje de daño en la nave, etc).

# Objetivo
Ejercitar la programación orientada a objetos, elaborar el proyecto a traves de la descripción por objetos: composición, agregación, herencia, etc.
Crear una carrera de naves mostrando sus estados y reconociendo el ganador.

# Construido con
El presente proyecto, se elaboró en base al lenguaje de programación Java, junto con el IDE ("Eclipse IDE for Java Developers"). El paquete de desarrollo utilizado fue la versión 8 de JDK.
* [Java] - Lenguaje de programación utilizado.
* [Eclipse](https://icb.utalca.cl/~fduran/eclipse-java-2019-03-R-linux-gtk-x86_64.tar.gz) - IDE de Eclipse, alojado en el repositorio de la carrera, Ingeniería Civil en Bioinformática - Universidad de Talca.

# Prerrequisitos
Para ejecutar correctamente el simulador, se debe tener instalado en el equipo el paquete de desarrollo de Java (JDK), puesto que es una programación escrita en Java. En caso de no contar con él, escribir:
```
apt install openjdk-8-jre
```

# Obtención del Programa
Para obtener este programa el primer paso es clonar el presente repositorio con el comando "git clone https://...".
Para compilar el juego se puede hacer a través de eclipse o terminal; en el caso de ser eclipse debe ingresar al repositorio del juego a través de este programa y luego ejecutarlo en el main. Por otra parte si se hace por terminal hemos de escribir lo siguiente: 
```
javac Main
```
A continuación, para ejecutar el programa en sí, escribir en la terminal:
```
java Main
```

# Sobre el código
El presente proyecto de programación consiste en la simulación de la Super Carrera Intergaláctica F-Zero, mediante la programación orientada a objetos. El objetivo del programa o "simulador", es competir contra otras dos naves. Existen varias caracteristicas con las que estas cuentan:
* *Modelo*
* *Cuerpo*
* *Combustible*

Cada una de estas caracteristicas es seleccionada de forma aleatoria por el programa e influye directamente en las estadísticas que pueda tener dentro del juego. Por ejemplo; el tipo de combustible afecta directamente en la velocidad de la nave por unidad de tiempo, mientras que tanto el modelo (actualmente de tres tipos: *Asalto, Lanzadera y de Exploración de mundos*) y el material con el que esta construida (procedente de todos los rincones del espacio: *Adamantium, Vibranium, Uru, Carbonadium y Promethium*) influyen tanto en su porcentaje de degradación como en la velocidad (y en consecuencia, también en su aceleración).

Por otra parte, la degradación de la nave, tambien se ve afectada por el tiempo que se demore en recorrer la pista de 10000 Km terrestres. Si el daño total es superior a un determinado porcentaje, se deberá detener para que el mecánico de la estación pueda realizar las reparaciones pertientes.

Finalmente, quien llegue primero a la meta obtiene la victoria y el juego se detiene, mostrando como resultados finales la lista completa de jugadores que compitieron en la carrera, que son emulados por la computadora.

# Errores del programa
- Existen problemas con la degradación (Class Mecanico no implementada temporalmente), ademas de la obtención de las posiciones de los jugadores al llegar a la meta (aunque si reconoce al ganador).

#Extras

De forma adicional existe el inicio de una propuesta visual (no implementada) para la visualización de la carrera alojada en el proyecto "Carrera_visual", ejecutable de la misma forma que el proyecto principal anteriormente descrito.

# Autores
Estudiantes de segundo año de Ingeniería Civil en Bioinformática.
* **Rachell Aravena**
* **Constanza Valenzuela**
