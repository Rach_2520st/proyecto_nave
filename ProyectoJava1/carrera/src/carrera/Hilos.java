package carrera;


public class Hilos {
	
	public static void main(String[] args) {
		System.out.println("***Bienvenido a carrera intergálactica***");
		System.out.println("Súmate a esta experiencia extraplanetaria");
		System.out.println("Si logras vences a tus oponentes podrás formar parte de los guardianes de la galaxia");
		System.out.println("¿Estás preparado para el reto?");
		Menu menu = new Menu();
		// Instancias
		Naveroja HiloNaveroja = new Naveroja();
		Naveverde HiloNaveverde = new Naveverde();
		// Como el hilo de la Naveverde fue implantado con Runnable
		Thread HV = new Thread(HiloNaveverde);
		Navecohete HiloNavecohete = new Navecohete();
		// Iniciar
		HiloNaveroja.start();
		HV.start();
		HiloNavecohete.start();
		
		System.out.println("Comienza la Carrera:");
	}

	
}
