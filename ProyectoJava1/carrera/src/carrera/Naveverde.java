package carrera;
//heredar de Thread como hilo de la Naveroja

public class Naveverde implements Runnable{
	
	public void run() {
		int i=0;
		long ms=0;
		System.out.println("Comienza la Nave Verde");
		while(i<5){
			try {
				ms = (long)(Math.random()*5+1*1000);
				Thread.sleep(ms);
				System.out.println("Nave Verde se mueve a " + ms + " Km/h");
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
			i++;
		}
		System.out.println("La Nave Verde llegó a la meta");
	}

}
