package proyecto;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

        String name;
        System.out.println("INGRESA TU NOMBRE");
        name = scanner.nextLine();

        if (name.isEmpty()) {
            System.out.println(Ansi.ANSI_YELLOW + "NO INGRESASTE UN NOMBRE");
            System.out.println("SE TE SELECCIONARA UN NOMBRE POR DETECTO" + Ansi.ANSI_RESET);
            name = "ANÓNIMO";
        }

        System.out.println("BIENVENIDO/A " + name);
        ArrayList<Corredor> corredores = new ArrayList();
        corredores.add(new Corredor(name));
        corredores.add(new Corredor("PLAYER2"));
        corredores.add(new Corredor("PLAYER3"));

        Menu menu = new Menu();
        menu.principal(); // refactor menuPrincipal->principal

        PistaCarrera pista = new PistaCarrera();
        //Km a recorrer; 10000
        pista.setLargo(10000);
        pista.setCorredores(corredores);

        //Mostrar la carrera
        pista.empezarCarrera();
        //Imprimir los lugares
        //pista.posiciones();
        
    }
}