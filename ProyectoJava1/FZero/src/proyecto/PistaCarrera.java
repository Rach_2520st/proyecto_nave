package proyecto;

import java.util.ArrayList;

public class PistaCarrera {
	private ArrayList<Corredor> corredores;
	private int largo;
	private boolean hayGanador = false;

	//public PistaCarrera() {
		// ...
	//}

	public ArrayList<Corredor> getCorredores() {
		return corredores;
	}

	public void setCorredores(ArrayList<Corredor> corredores) {
		this.corredores = corredores;
	}

	public int getLargo() {
		return largo;
	}

	public void setLargo(int largo) {
		this.largo = largo;
	}

	public void empezarCarrera() {
		for (Corredor corredor : this.corredores) {
			// al comienzo de la carrera,
			// hacer algo con cada corredor
			corredor.setPista(this);
			
			//Informacion sobre los corredores
			System.out.println("INFORMACION CORREDORES");
			corredor.presentacion();
		}

		this.correrCarrera();
	}

	public void correrCarrera() {
		// segundo sólo para mostrar algo bonito
		int segundo = 0;

		// aquí la carrera avanza
		while(true) {
			segundo += 1;
			System.out.println("==============");
			System.out.println("Tiempo transcurrido: " + segundo + " segundos");

			
			// por cada corredor...
			for (Corredor corredor : this.corredores) {
				corredor.nuevoTiempo(segundo);
				// avanzar un poco
				corredor.avanzar();

				// Mostrar posicion + info(status)
				corredor.informacion();
			}

			if (this.hayGanador) {
				// TODO (arreglar que se detenga cuando lleguen todos)
				// alguien ya pasó la meta
				System.out.println(Ansi.ANSI_RED + "***CARRERA FINALIZADA***" + Ansi.ANSI_RESET);
				break;
			}
			// Esperar 1000 ms después de avanzar un poco a todos los corredores
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void cruzarMeta(Corredor corredor) {

		//int contador = 0;
		if (this.hayGanador == true) {
			//contador +=1;
			System.out.println("HA LLEGADO A LA META " + corredor.getNombre());
			return;
		}

		this.hayGanador = true;
		System.out.println("¡FELICIDADES " + corredor.getNombre() + "! GANASTE LA CARRERA");
		//String primerLugar = corredor.getNombre();
		
	}
	
	public void posiciones(String primerLugar) {
		System.out.println("");
		System.out.print("PREMIACION\n");
		System.out.println("Primer Lugar: "+ primerLugar);
		/*if(distancia1 > distancia2) {
		System.out.println("Segundo Lugar: "+ c1);
		System.out.println("Tercer Lugar: "+ c2);
		}else {
			System.out.println("Segundo Lugar: "+ c2);
			System.out.println("Tercer Lugar: "+ c1);
		}*/
	}

}
