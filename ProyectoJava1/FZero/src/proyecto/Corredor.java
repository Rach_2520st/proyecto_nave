package proyecto;
import proyecto.PistaCarrera;

public class Corredor{

	private float distancia;
	private String nombre;
	private String modelo;
	private String cuerpo;
	private String combustible;
	private PistaCarrera pista;
	
	private float velocidad;
	// es 1 - X porque son como los puntos de vida.
	// si quieres una velocidad 90 a partir de la velocidad 100, el daño tiene
	// que ser 10%, entonces da que    1 - 0.10 = 0.90
	// por lo tanto, velocidad * degradacion = 100 * 0.90 = 90
	private float degradacion = 1 - 0.10f; //1f = 100%, 0.01f = 1%
	private float aceleracion;
	private int segundo;
	
	public Corredor(String nombre) {
	
		this.distancia = 0; //distancia inicial 0 s
		this.setNombre(nombre);
		
		// Azar para modelo
		int randomModelo = random(1,3);		

		if (randomModelo == 1) {
			this.modelo = "Asalto";
			this.velocidad += 2;
			this.degradacion += (float) 20.0;
		} else if (randomModelo == 2) {
			this.modelo = "Lanzadera";
			this.velocidad += 1;
			this.degradacion += (float) 10.0;
		} else if (randomModelo == 3) {
			this.modelo = "Exploracion";
			this.velocidad += 0.5;
			this.degradacion += (float) 5.0;
		}

		// Azar para material
		int randomCuerpo = random(1,5);

		if (randomCuerpo == 1) {
			this.cuerpo = "Adamantium";
			this.velocidad += 0.4;
			this.degradacion += 2.0;
		} else if (randomCuerpo == 2) {
			this.cuerpo = "Vibranium";
			this.velocidad += 0.5;
			//mejor material menos daño
			this.degradacion += 1.0;
		} else if (randomCuerpo == 3) {
			//peor material mayor daño
			this.cuerpo = "Uru";
			this.velocidad += 0.1;
			this.degradacion += 5.0;
		} else if (randomCuerpo == 4) {
			this.cuerpo = "Carbonadium";
			this.velocidad += 0.2;
			this.degradacion += 3.0;
		} else if (randomCuerpo == 5) {
			this.cuerpo = "Promethium";
			this.velocidad += 0.3;
			this.degradacion += 4.0;
		}

		// Azar para combustible
		int randomCombustible = random(1,3);
		
		if (randomCombustible == 1) {
			this.combustible = "Biodiesel";
			this.velocidad = (int) (velocidad * 1.04); // 100% + 4%
		}
		else if (randomCombustible == 2) {
			this.combustible = "Nuclear";
			this.velocidad = (int) (velocidad * 1.05); // 100% + 5%
		}
		else if (randomCombustible == 3) {
			this.combustible = "Diesel";
			this.velocidad = (int) (velocidad * 1.03); // 100% + 3%
		}
		
	}

	public void avanzar() {
		float avance = random(0, 10) + this.velocidad * this.getDegradacion();

		this.distancia += avance;

		if (this.distancia >= pista.getLargo()) {
			//corredor cruza la meta
			pista.cruzarMeta(this);
		}
	}

	public void informacion() {		
		
		// Formulas!
		this.degradacion = (this.degradacion * this.segundo);
		// en caso de la degradacion ser mayor a la esperada, dejarla en 100%()
		if (degradacion >= 100.0){
			this.degradacion = (float) 100.0;
		}
		this.velocidad = (this.distancia * this.segundo) / this.degradacion;
		this.aceleracion = this.velocidad * this.segundo;

		// llamar al mecanico
		if(degradacion >= 80.0) {
			System.out.println("[" + this.getNombre() + "] ¡ALERTA! DAÑO CRITICO, LLAMANDO A LA MECANICO");
			//Mecanico.reparacion(degradacion);
			//this.degradacion=degradacion;
			//Mecanico.distanciaReparacion(this.distancia);
		
		}
		
		System.out.println("[" + this.getNombre() + "] Distancia actual:   " + this.distancia + " Km");
		System.out.println("[" + this.getNombre() + "] Velocidad actual:   " + this.velocidad * this.getDegradacion() + " Km/s");
		System.out.println("[" + this.getNombre() + "] Aceleración actual: " + this.aceleracion + " Km/s²");
		System.out.println("[" + this.getNombre() + "] Degradación (%):    " + this.degradacion);
		
	}
	
	public void presentacion() {
		System.out.println("[" + this.getNombre() + "]");
		System.out.println("Nave:                  " + nombre );
		System.out.println("Modelo:                " + modelo);
		System.out.println("Material:              " + cuerpo);
		System.out.println("Tipo de Combustible:   " + combustible);
		System.out.println("Degradación (%):         " + degradacion);	
		System.out.println("");
	}

	//Para los random
	private int random(int minimo, int maximo) {
		return minimo + (int)(Math.random() * ((maximo - minimo) + 1));
	}

	public PistaCarrera getPista() {
		return pista;
	}

	public void setPista(PistaCarrera pista) {
		this.pista = pista;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public float getDegradacion() {
		return degradacion;
	}

	public void setDegradacion(float degradacion) {
		this.degradacion = degradacion;
	}

	public void nuevoTiempo(int segundo) {
		this.segundo = segundo;
		
	}
	
}
