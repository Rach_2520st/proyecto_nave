package proyecto;

import java.util.Scanner;

public class Menu {
    private Scanner scanner;

	public Menu() {
    	
        System.out.println("");
        //Pantalla de estrellas
        System.out.println("* * * * * * * * * * * * * * * * * * * * ");
        System.out.println(" * * * * * * * * * * * * * * * * * * * *");
        System.out.println("* * * * * * * * * * * * * * * * * * * * ");
        System.out.println(" * * * * * * * * * * * * * * * * * * * *\n");

        System.out.println( "  ------------------------------------");
        System.out.println(Ansi.ANSI_CYAN + "      SUPER CARRERA INTERGALÁCTICA      " + Ansi.ANSI_RESET);
        System.out.println( "  ------------------------------------" );
        System.out.println("SÚMATE A ESTA EXPERIENCIA EXTRAPLANETARIA");
        System.out.println("¿ESTÁS LISTO?");
    }

    public void principal() {
        System.out.println("\n         [1] COMIENZA LA CARRERA\n         [2] SALIR");

        scanner = new Scanner(System.in);
		int opcion = scanner.nextInt();

        if (opcion != 1) {
            System.out.println("ADIÓS");
            System.exit(0);
        }
    }
}