
public class Ala {
	private String lateralidad;
	private String  tipoAla; //alas delta, flechas, elíptica
	
	public Ala(String lateralidad, String tipoAla) {
		this.lateralidad = lateralidad;
		this.tipoAla = tipoAla;
	}
	
	public String Lateralidad() {
		return this.lateralidad;
	}
	
	public String TipoAla() {
		return this.tipoAla;
	}
}
