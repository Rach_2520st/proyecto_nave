
public class Odometro {
    private double  distanciaRecorrida;
    private double velocidad;
    private double aceleracion;
    
    public Odometro() {
        // Inicio en cero (salida)
        distanciaRecorrida = 0;
    }
    
    
    public double getDistanciaRecorrida() {
        return this.distanciaRecorrida;
    }
    
    public void resetearOdemotro() {
        // Reseteo en 0 para el caso de una nueva carrera 
        distanciaRecorrida = 0;
    }
    
  

    
    
    
    public void setVelocidad(double velocidad) {
        this.velocidad = velocidad;
    }
    public double getVelocidad() {
        return this.velocidad;
    }
    
    public double getAceleracion() {
	return aceleracion;
    }
 
    public void setAceleracion(double aceleracion) {
    	this.aceleracion = aceleracion;
	}

	public double medirVelocidad(double velocidadActual, double velocidad){
        this.velocidad = velocidad + velocidadActual;
        this.aceleracion = this.velocidad - velocidad;
        this.distanciaRecorrida = this.distanciaRecorrida + this.velocidad;
        return this.velocidad;
    } 
    
}
