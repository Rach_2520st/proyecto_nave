
public class Nave {
	// Nave compuesta de estas 3 clases
	// protect permite que cuando lo heredo no cuando lo instancio 2 clases()
	protected Estanque estanque;
	protected Odometro odometro;
	protected Motor motor;
	
	// Constructor para cada nave (armando)
	public Nave() {
		estanque = new Estanque();
		odometro = new Odometro();
		motor = new Motor();
	}
	
	
	// No set porque los construyo desde 0
	public Odometro getOdometro() {
		return this.odometro;
	}
	
	public Estanque getEstanque() {
		return this.estanque;
	}
	
	public Motor getMotor() {
		return motor;
	}
	
	public void addKilometroUso(int kilometro) {
		motor.setKilometrosUso(kilometro);
	}
	
	
}
