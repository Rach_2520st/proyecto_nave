import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Carrera {
	// Array que contiene a los aviones 
	private ArrayList<AvionCarrera> Aviones = new ArrayList<AvionCarrera>();
	private Locutor locutor = new Locutor();
	private boolean InicioCarrera;
	private int tiempoCarrera;
	private int competidores;
	
	public Carrera(){
		InicioCarrera=false;
		
	}
	
	public void IniciarCarrera(){
		
		// No hay aviones, no puede empezar
		if(Aviones.size() == 0){
			locutor.mensajePublico("No existen competidores en la Carrera");	
			competidores = 0;
		}
		
		// Es como si ya se hubiese iniciado la carrera
		if(InicioCarrera==true){
			locutor.mensajePublico("La Carrera ya inicializada!");	
			return;
		}

		competidores = 0;
		InicioCarrera = true;
		// Agregar competidores
		AgregarCompetidores();
		// Presentar competidores
		locutor.exponerCompetidores(Aviones);
		// Inicializar pista
		AutoPista pista = new AutoPista(500);
		encenderNaves();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		locutor.mensajePublico("\n🏁 🏁  ¡INICIA LA CARRERA! 🏁 🏁 ");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		//iniciar tiempo en cero
		tiempoCarrera = 0;
		
		AvionCarrera lider = null;
		AvionCarrera liderAnterior = null;
		
		while(locutor.getPosicion() < Aviones.size()) {
			
			System.out.printf("\n««««««««««««««« Tiempo Actual " + tiempoCarrera + " s »»»»»»»»»»»»»»»\n");
			tiempoCarrera += 1; //mostrar cada 1 segundo
			
			//informar cuantas naves se encuentran en la meta hasta ese segundo
			locutor.mensajePublico("Hay " + locutor.getPosicion() + " naves en la meta.");
			System.out.println();
			
			
			for (AvionCarrera _avion: Aviones) {

				//si no se esta reparando y no esta en la meta entonces esta corriendo
				//mostrar informacion
				if(_avion.getEnAsistencia()==false && _avion.getPosicion() == 0) {
					double velocidadActual;
					velocidadActual = _avion.medirVelocidad(_avion.getOdometro().getVelocidad());
					double deterioro = _avion.getMotor().efectoMotor(velocidadActual);
					double aceleracionActual = _avion.getOdometro().getAceleracion();
					
					locutor.mensajePublico("\nNave " + _avion.getNroCompetidor() + "(" +  _avion.getNombrePiloto() + ")");
					locutor.imprimirImagen(_avion.getOdometro().getDistanciaRecorrida(), pista.getLargoAutopista());
					locutor.mensajePublico("Distancia:     " + _avion.getOdometro().getDistanciaRecorrida()  + " km");
					locutor.mensajePublico("Velocidad:     " + velocidadActual + " km/s");
					locutor.mensajePublico("Aceleracion:   " + aceleracionActual  + " km/s²");
					locutor.mensajePublico("Daño en motor: " + deterioro + "%");
					
					if(deterioro >= 30.0) {
						//respuesta al daño
						_avion.setEnAsistencia(true);
					}
					// Autopista verifica llegada a meta
					if(pista.llegoMeta((int) _avion.getOdometro().getDistanciaRecorrida())){
						locutor.addLugares(_avion);
						System.out.println();
						// Sobrecarga a avionCarrera, clase se encarga de su propia mensajeria
						// es como sobreescribirlo
						locutor.mensajePublico(_avion.toString());
					}	
				}
				
				if(_avion.getEnAsistencia()==true) {
					
					locutor.addComentario("[" + tiempoCarrera + " s] ⚠️ ¡Daño crítico! Nave Auxiliar reparando a " + _avion.getNroCompetidor() + "!");
					//llamar a reparaciones de la nave asistente
					
				}

			 /*Collections.sort(Aviones, new Comparator<AvionCarrera>() {
					@Override
					public int compare(AvionCarrera arriba, AvionCarrera abajo) { //comparando 2 naves
						if (_avion.arriba.getDistanciaRecorrida() < abajo._avion.getOdometro().getDistanciaRecorrida()) // donde x() es la distancia en ese segundo
							return 1;
						else if (left._avion.getOdometro().getDistanciaRecorrida() > right._avion.getOdometro().getDistanciaRecorrida())
							return -1;
						else
							return 0;
					}
				});*/
			}
			
			lider = Aviones.get(0);
			System.out.print("\nComentarios del Locutor:");
			if (lider != liderAnterior && lider != null) {
				if (liderAnterior != null) {
					locutor.addComentario("[" + tiempoCarrera + "s] " + lider.getNroCompetidor() + " sobrepasa a " + liderAnterior.getNroCompetidor() + "!");
				} else {
					locutor.addComentario("[" + tiempoCarrera + "s] " + lider.getNroCompetidor() + " lidera la carrera!");
				}

				liderAnterior = lider;
			}

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			// buscar los autos que tengan daños y hacerlos volver a la carrera
			for(AvionCarrera _avion: Aviones){
				if (_avion.getEnAsistencia()){
				_avion.motor.efectoMotor(0);
				_avion.setEnAsistencia(false);
				}
			}
		}
		
		locutor.mensajePublico("\nLa duración de la emocionante carrera fue de " + tiempoCarrera + " segundos");
		locutor.premiacionNaves();
	}
	
	private void encenderNaves(){
		// Arranque de las naves
		  for (AvionCarrera _avion: Aviones) {
			  _avion.encenderNave();
		  }
	}
	
	
	public void AgregarCompetidores() {
		if(InicioCarrera==true){
			// Idea inicia para agregar N naves
			/*String res = "S";
			while(res.equals("S") || res.equals("s")) {
				Scanner sc = new Scanner(System.in);
				AvionCarrera nave = new AvionCarrera();
				System.out.println();
				System.out.println("----------Datos del competidor -------------------------------");
				System.out.println("Nombre Piloto:");
				nave.setNombrePiloto(sc.nextLine());
				System.out.println("Nro de Nave:");
				nave.setNroCompetidor(sc.nextInt());
				sc.nextLine();
				System.out.println("Patente Nave: ");
				nave.setPatente(sc.nextLine());
				nave.getOdometro().resetearOdemotro();
				Aviones.add(nave);
				
				System.out.println();
				System.out.println("Desea ingresar más competidores a la carrera? (S/N)");
				res = sc.nextLine();
			}*/
			
			// Para 2 naves
			int i = 1;
			do {
				Scanner sc = new Scanner(System.in);
				AvionCarrera nave = new AvionCarrera();
				System.out.println();
				System.out.println("----------Datos del competidor -------------------------------");
				System.out.println("Nombre Piloto:");
				nave.setNombrePiloto(sc.nextLine());
				System.out.println("Nro de Nave:");
				nave.setNroCompetidor(i);
				System.out.println(i);
				System.out.println("Patente Nave: ");
				nave.setPatente(sc.nextLine());
				nave.getOdometro().resetearOdemotro();
				Aviones.add(nave);
				
				System.out.println();
				i++;
			}while(i < 3);
		}
	}
	
}