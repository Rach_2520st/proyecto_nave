
public class Motor {
	private int nroChasis;
	private int caballosFuerza;
	private int kilometrosUso;
	//private boolean estadoMotor; // Para ver el daño, no fue necesario
	private TipoCombustible tipoCombustible; // Asociado a motor
	private double nivelDeteriodo;

	public Motor(){
		// Inicio en cero y obtencion del combustible
		kilometrosUso = 0;
		nivelDeteriodo = 0;
		// Combustible aleatprio
		this.tipoCombustible =  TipoCombustible.asignarTipo((int) (Math.random() * 3) + 1);
	}
	
	public String getNombreCombustible(){
		return TipoCombustible.NombreCombustible(this.tipoCombustible);
	}
	
	public TipoCombustible tipoCombustible() {
		return this.tipoCombustible;
	}
	
	
	public void setKilometrosUso(int kilometrosUso) {
		this.kilometrosUso += kilometrosUso;
	}
	
	public int getKilometrosUso(){
		return this.kilometrosUso;
	}
	
	public void setNroChasis(int nroChasis){
		this.nroChasis = nroChasis;
	}
	
	// Encapsulamiento de Atributos getters
	public int getNroChasis() {
		return this.nroChasis;
	}
	
	public void setCaballosFuerza(int caballosFuerza) {
		this.caballosFuerza= caballosFuerza;
	}
	
	// Encapsulamiento de Atributos getters
	public int getCaballosFuerza() {
		return this.caballosFuerza;
	}
	
	public void setNiveldeteriodo(int nivelDeteriodo) {
		this.nivelDeteriodo= nivelDeteriodo;
	}
	
	public double getNiveldeteriodo() {
		return this.nivelDeteriodo;
	}
	
	//  Velocidad inicial
	public double velocidadArranque (){
		int velocidad;
		
		if(this.tipoCombustible.equals(TipoCombustible.NUCLEAR)){
			velocidad = 20;
		}
		else if(this.tipoCombustible.equals(TipoCombustible.DIESEL)) {
			velocidad = 10;
		}
		else if(this.tipoCombustible.equals(TipoCombustible.BIODIESEL)) {
			velocidad = 5;
		}
		else {
			velocidad = 0;
		}
		return velocidad;
	}

	public double fuerzaEmpuje (double velocidad) {
		
		if(tipoCombustible.equals(TipoCombustible.NUCLEAR)){
			return (velocidad*(0.5));
		}
		else if(tipoCombustible.equals(TipoCombustible.DIESEL)) {
			return (velocidad*(0.25));

		}
		else if(tipoCombustible.equals(TipoCombustible.BIODIESEL)) {
			return (velocidad*(0.10));

		}
		return 1;
	}
	
	public double efectoMotor (double velocidad) {
		if(tipoCombustible.equals(TipoCombustible.NUCLEAR)){
			nivelDeteriodo  = nivelDeteriodo + (velocidad*(0.05));
		}else if(tipoCombustible.equals(TipoCombustible.DIESEL)) {
			nivelDeteriodo = nivelDeteriodo + (velocidad*(0.025));
		}else if(tipoCombustible.equals(TipoCombustible.BIODIESEL)) {
			nivelDeteriodo = nivelDeteriodo +  (velocidad*(0.01));
		}
		return nivelDeteriodo;
	}

}
