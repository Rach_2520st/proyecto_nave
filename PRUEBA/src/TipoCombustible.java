// Un enumerado (o Enum) clase que limita la creación de objetos 
// a los especificados explícitamente en la implementación de la clase.
public enum TipoCombustible{
	NUCLEAR, DIESEL,BIODIESEL;
	
	public static TipoCombustible asignarTipo(int RdnTipoCombustible){
		switch (RdnTipoCombustible) {
			case 1:
				return NUCLEAR;
			case 2:
				return DIESEL;
			case 3:
				return BIODIESEL;
			default:
				throw new AssertionError("Error, aceleración no definida!");
		}
	}
	
	public static String NombreCombustible(TipoCombustible E){
		switch (E) {
			case NUCLEAR:
				return "Nuclear";
			case DIESEL:
				return "Diesel";
			case BIODIESEL:
				return "Biodiesel";
			default:
				throw new AssertionError("Error, combustible no definido!" );
		}
	}
	
}
