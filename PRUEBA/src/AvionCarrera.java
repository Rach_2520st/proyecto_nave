
// Hereda propiedades de la nave
public class AvionCarrera extends Nave {
	private Ala ala1;
	private Ala ala2;
	private int nroCompetidor; // Identificacion
	private String nombrePiloto;
	private String patente;
	private boolean enAsistencia;
	private int posicion;
	
	public AvionCarrera(){
		ala1 = new Ala("Derecha","Delta");
		ala2 = new Ala("Izquierda","Delta");
		enAsistencia= false;
		posicion= 0;
	}
	
	// Solo cuando se instalan las naves en la pista, por eso el get y set
	public void setNroCompetidor(int nroCompetidor) {
		this.nroCompetidor= nroCompetidor;
	}
	
	public void setEnAsistencia(boolean estaAsistencia)
	{
		this.enAsistencia = estaAsistencia;
	}
	
	public boolean getEnAsistencia()
	{
		return this.enAsistencia;
	}
	
	public int getNroCompetidor() {
		return this.nroCompetidor;
	}
	
	// Calculados en base a distancia
	//FIX TODO
	public int getKilometrosUso() {
		return 7;
	}
	
	public void setNombrePiloto(String nombrePiloto) {
		this.nombrePiloto= nombrePiloto;
	}
	
	public String getNombrePiloto() {
		return this.nombrePiloto;
	}
	
	public void setPatente(String patente) {
		this.patente= patente;
	}
	
	public String getPatente() {
		return this.patente;
	}
	
	public void encenderNave(){
		getOdometro().setVelocidad(this.getMotor().velocidadArranque());
		//getOdometro().setAceleracion(this.getMotor().aceleracionArranque());
	}
	
	public void setPosicion(int posicion){
		 this.posicion = posicion;
	}
	
	public int getPosicion() {
		return this.posicion;
	}
	
	public double medirVelocidad(double velocidad){
		return getOdometro().medirVelocidad(getMotor().fuerzaEmpuje(velocidad), velocidad);
	}
	
	@Override
	public String toString(){
		return "La nave " + this.nroCompetidor + " pilotada por " + this.nombrePiloto + " ha llegado a la meta.";
	}
	
}
