import java.util.ArrayList;
import java.util.Collections;

public class Locutor {

	private ArrayList<String> mensaje = new ArrayList<String>();
	private ArrayList<AvionCarrera> Ganadores = new ArrayList<AvionCarrera>();
	private int posicion;

	public Locutor(){
		posicion=0;
	}
	
	public void comentarios() {
		ArrayList<String> mensajeCopia = new ArrayList<String>(mensaje);
		Collections.reverse(mensajeCopia);

		for (int i = 0; i < mensajeCopia.size(); i++) {
			System.out.printf(" - " + mensajeCopia.get(i));

		}
	}
	
	// Imprime una barra de progreso 
	// 100 puntos representan el 100% del largo de pista
	public void imprimirImagen(double distancia, float largo) {
		float porcentaje = (float) (distancia / largo * 100);
		String camino = "";

		for (int i = 0; i < 100; i++) {
			if (i <= porcentaje) {
				camino += "✈"; // Un mini avion porque la nave 🚀 es muy grande :c
			} else {
				camino += ".";
			}
		}

		System.out.print("[" + camino + "]\n");
	}
	
	
	public void exponerCompetidores(ArrayList<AvionCarrera> Aviones){
		System.out.println("\n««««««««««««««« Naves en Competencia »»»»»»»»»»»»»»»\n");
		
		for (AvionCarrera nv: Aviones) {
			
			System.out.println("Piloteada por		" + nv.getNombrePiloto());
			System.out.println("Nro Nave:			" + nv.getNroCompetidor());
			System.out.println("Patente Nave		" + nv.getPatente());
			System.out.println("Tipo Combustible	" + nv.getMotor().getNombreCombustible());
			System.out.println("---------------------------------------------");
			
		}
	}
	
	public void mensajePublico(String comentario){
		System.out.println(comentario);
	}
	
	public void addLugares(AvionCarrera avion){
		avion.setPosicion(posicion += 1);
		Ganadores.add(avion);
		
	}
	
	public void premiacionNaves(){
		System.out.println("\n🏁 🏁 ¡Todas las naves alcanzaron la meta! 🏁 🏁\n");
		System.out.println("      🎖️🎖️🎖️      PREMIACION      🎖️🎖️🎖️      ");
		for (AvionCarrera nv: Ganadores) {
			System.out.println("🎖️#" + nv.getPosicion() + " El avión piloreado por " + nv.getNombrePiloto() + " que perticipó con el Nro:" + nv.getNroCompetidor());
			//TODO
			System.out.println("     - Daño: " + nv.motor.getNiveldeteriodo() + " %");
			
		}
	}
	
	public int getPosicion(){
        return this.posicion;
    }

	public void addComentario(String msj) {
		// TODO Auto-generated method stub
		mensaje.add(msj);
		
	}
}
