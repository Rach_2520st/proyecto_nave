//import java.util.ArrayList;

public class AutoPista {
	private int largoAutopista;
	//private String materialAutoPista;//idea para mas complejidad
	//private ArrayList<AvionCarrera> meta = new ArrayList<AvionCarrera>(); //idea para guardar aviones

	public AutoPista(int largoAutopista){
		this.largoAutopista = largoAutopista;
	}
	
	
	public int getLargoAutopista() {
		return largoAutopista;
	}

	public void setLargoPista(int largoAutopista) {
		this.largoAutopista= largoAutopista;
	}

	/**
	 * public ArrayList<AvionCarrera> getMeta() {
	 *	return meta;
	 * }
	 *
	 * public void setMeta(ArrayList<AvionCarrera> meta) {
	 *  this.meta = meta;
	 * }
	 *
	 * public void addMeta(AvionCarrera nv) {
	 *	this.meta.add(nv);
	 * }
	 **/
	
	public boolean llegoMeta(int tramoRecorrido){
		if(tramoRecorrido <= this.largoAutopista)
			return false;
		else
			return true;
	}
	
}
